/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   utils.h
 * Author: crewgeracid
 *
 * Created on December 12, 2017, 1:19 PM
 */

#ifndef UTILS_H
#define UTILS_H

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include <tgl/tgl.h>
#include <tgl/tgl-structures.h>
#include <tgl/tgl-layout.h>

void escape_string(const char *in, char *out);
void get_date_time_formatted(long t, char *out);
void get_tgl_file_json(struct tgl_file_location file, char *out);
void get_tgl_media_json(struct tgl_message_media *M, char *out);
void get_tgl_peer_json(struct tgl_state *TLS, tgl_peer_id_t peer, char *out);

#endif /* UTILS_H */

