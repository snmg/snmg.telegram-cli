#include "utils.h"

void escape_string(const char *in, char *out) {
    char c;
    while ((c = *(in++))) {
        switch (c) {
            case '\n':
                *(out++) = '\\';
                *(out++) = 'n';
                break;
            case '\r':
                *(out++) = '\\';
                *(out++) = 'r';
                break;
            case '\\':
                *(out++) = '\\';
                *(out++) = '\\';
                break;
            case '\"':
                *(out++) = '\\';
                *(out++) = '\"';
                break;
            default:
                *(out++) = c;
        }
    }
    *out = '\0';
}

void get_date_time_formatted(long t, char *out) {
    struct tm *tm = localtime((void *) &t);
    sprintf(out, "%02d.%02d.%04d %02d:%02d:%02d", tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900, tm->tm_hour, tm->tm_min, tm->tm_sec);
}

void get_tgl_file_json(struct tgl_file_location file, char *out) {
    if (!file.local_id) {
        sprintf(out, "null");
        return;
    }
    sprintf(out,
            "{\"local_id\":%d, \"dc\":%d, \"secret\":%lld, \"volume\":%lld}",
            file.local_id, file.dc, file.secret, file.volume);
}

void get_tgl_media_json(struct tgl_message_media *M, char *out) {
    if (M->type == tgl_message_media_none) {
        sprintf(out, "null");
        return;
    }
    char media_type[16];
    long long media_id;
    char media_caption[M->caption && strlen(M->caption) ? (sizeof (M->caption) + 1)*2 : 6];
    char media_resource_caption[4096];
    char media_special_data[4096];
    if (M->caption && strlen(M->caption)) {
        char escaped[sizeof (M->caption)*2];
        escape_string(M->caption, escaped);
        sprintf(media_caption, "\"%s\"", escaped);
        memset(escaped, 0, sizeof (escaped));
    } else {
        sprintf(media_caption, "null");
    }
    switch (M->type) {
        case tgl_message_media_photo:
            sprintf(media_type, "\"photo\"");
            media_id = M->photo->id;
            (M->photo->caption && strlen(M->photo->caption)) ? sprintf(media_resource_caption, "\"%s\"", M->photo->caption) : sprintf(media_resource_caption, "null");
            sprintf(media_special_data, "null");
            break;
        case tgl_message_media_document:
        case tgl_message_media_audio:
        case tgl_message_media_video:
            if (M->document->flags & TGLDF_IMAGE) {
                if (M->document->caption && strlen(M->document->caption) && strcmp(M->document->caption, "sticker.webp") == 0) {
                    // TGLDF_STICKER don't work
                    sprintf(media_type, "\"sticker\"");
                } else {
                    sprintf(media_type, "\"image\"");
                }
            } else if (M->document->flags & TGLDF_AUDIO) {
                sprintf(media_type, "\"audio\"");
            } else if (M->document->flags & TGLDF_VIDEO) {
                sprintf(media_type, "\"video\"");
            } else if (M->document->flags & TGLDF_STICKER) {
                // TGLDF_STICKER don't work
                sprintf(media_type, "\"sticker\"");
            } else {
                sprintf(media_type, "\"document\"");
            }
            media_id = M->document->id;
            (M->document->caption && strlen(M->document->caption)) ? sprintf(media_resource_caption, "\"%s\"", M->document->caption) : sprintf(media_resource_caption, "null");
            sprintf(media_special_data, "null");
            break;
        case tgl_message_media_document_encr:
            if (M->encr_document->flags & TGLDF_IMAGE) {
                if (M->encr_document->caption && strlen(M->encr_document->caption) && strcmp(M->encr_document->caption, "sticker.webp") == 0) {
                    // TGLDF_STICKER don't work
                    sprintf(media_type, "\"sticker\"");
                } else {
                    sprintf(media_type, "\"image\"");
                }
            } else if (M->encr_document->flags & TGLDF_AUDIO) {
                sprintf(media_type, "\"audio\"");
            } else if (M->encr_document->flags & TGLDF_VIDEO) {
                sprintf(media_type, "\"video\"");
            } else if (M->encr_document->flags & TGLDF_STICKER) {
                // TGLDF_STICKER don't work
                sprintf(media_type, "\"sticker\"");
            } else {
                sprintf(media_type, "\"document\"");
            }
            media_id = M->encr_document->id;
            (M->encr_document->caption && strlen(M->encr_document->caption)) ? sprintf(media_resource_caption, "\"%s\"", M->encr_document->caption) : sprintf(media_resource_caption, "null");
            sprintf(media_special_data, "null");
            break;
        case tgl_message_media_geo:
            sprintf(media_type, "\"geo\"");
            media_id = -1;
            sprintf(media_resource_caption, "null");
            sprintf(media_special_data,
                    "{\"lat\":%lf, \"lng\":%lf}",
                    M->geo.latitude, M->geo.longitude);
            break;
        case tgl_message_media_venue:
            sprintf(media_type, "\"venue\"");
            media_id = -1;
            (M->venue.title && strlen(M->venue.title)) ? sprintf(media_resource_caption, "\"%s\"", M->venue.title) : sprintf(media_resource_caption, "null");
            sprintf(media_special_data,
                    "{\"id\":\"%s\", \"title\":\"%s\", \"address\":\"%s\", \"provider\":\"%s\", \"lat\":%lf, \"lng\":%lf}",
                    M->venue.venue_id, M->venue.title, M->venue.address, M->venue.provider, M->venue.geo.latitude, M->venue.geo.longitude);
            break;
        case tgl_message_media_contact:
            sprintf(media_type, "\"contact\"");
            media_id = -1;
            sprintf(media_resource_caption, "null");
            sprintf(media_special_data,
                    "{\"id\":\"%d\", \"phone\":\"%s\", \"firstname\":\"%s\", \"lastname\":\"%s\"}",
                    M->user_id, M->phone, M->first_name, M->last_name);
            break;
        case tgl_message_media_webpage:
            sprintf(media_type, "\"webpage\"");
            media_id = -1;
            sprintf(media_resource_caption, "null");
            sprintf(media_special_data,
                    "{\"url\":\"%s\", \"title\":\"%s\", \"description\":\"%s\", \"author\":\"%s\"}",
                    M->webpage->url, M->webpage->title, M->webpage->description, M->webpage->author);
            break;
        default:
            sprintf(out, "null");
            return;
    }
    sprintf(out,
            "{\"id\":%lld, \"type\":%s, \"caption\":%s, \"resource_caption\":%s, \"special_data\":%s}",
            media_id, media_type, media_caption, media_resource_caption, media_special_data);
}

void get_tgl_peer_json(struct tgl_state *TLS, tgl_peer_id_t peer, char *out) {
    if (!peer.peer_id || !peer.peer_type || peer.peer_type > TGL_PEER_CHANNEL) {
        sprintf(out, "null");
        return;
    }
    tgl_peer_t *peer_full = tgl_peer_get(TLS, peer);
    char peer_type[15];
    char peer_username[255];
    char peer_firstname[255];
    char peer_lastname[255];
    char peer_phone[255];
    char peer_photo[4096 + 100];
    char photo_big_json[2048];
    char photo_small_json[2048];
    switch (peer.peer_type) {
        case TGL_PEER_USER:
            strcpy(peer_type, "\"user\"");
            (peer_full->user.username && strlen(peer_full->user.username)) ? sprintf(peer_username, "\"%s\"", peer_full->user.username) : sprintf(peer_username, "null");
            (peer_full->user.first_name && strlen(peer_full->user.first_name)) ? sprintf(peer_firstname, "\"%s\"", peer_full->user.first_name) : sprintf(peer_firstname, "null");
            (peer_full->user.last_name && strlen(peer_full->user.last_name)) ? sprintf(peer_lastname, "\"%s\"", peer_full->user.last_name) : sprintf(peer_lastname, "null");
            (peer_full->user.phone && strlen(peer_full->user.phone)) ? sprintf(peer_phone, "\"%s\"", peer_full->user.phone) : sprintf(peer_phone, "null");
            get_tgl_file_json(peer_full->user.photo_big, photo_big_json);
            get_tgl_file_json(peer_full->user.photo_small, photo_small_json);
            sprintf(peer_photo, "{\"photo_big\":%s, \"photo_small\":%s}", photo_big_json, photo_small_json);
            break;
        case TGL_PEER_CHAT:
            strcpy(peer_type, "\"chat\"");
            (peer_full->chat.title && strlen(peer_full->chat.title)) ? sprintf(peer_username, "\"%s\"", peer_full->chat.title) : sprintf(peer_username, "null");
            sprintf(peer_firstname, "null");
            sprintf(peer_lastname, "null");
            sprintf(peer_phone, "null");
            get_tgl_file_json(peer_full->chat.photo_big, photo_big_json);
            get_tgl_file_json(peer_full->chat.photo_small, photo_small_json);
            sprintf(peer_photo, "{\"photo_big\":%s, \"photo_small\":%s}", photo_big_json, photo_small_json);
            break;
        case TGL_PEER_ENCR_CHAT:
            strcpy(peer_type, "\"secret_chat\"");
            (peer_full->encr_chat.print_name && strlen(peer_full->encr_chat.print_name)) ? sprintf(peer_username, "\"%s\"", peer_full->encr_chat.print_name) : sprintf(peer_username, "null");
            sprintf(peer_firstname, "null");
            sprintf(peer_lastname, "null");
            sprintf(peer_phone, "null");
            get_tgl_file_json(peer_full->encr_chat.photo_big, photo_big_json);
            get_tgl_file_json(peer_full->encr_chat.photo_small, photo_small_json);
            sprintf(peer_photo, "{\"photo_big\":%s, \"photo_small\":%s}", photo_big_json, photo_small_json);
            break;
        case TGL_PEER_CHANNEL:
            strcpy(peer_type, "\"channel\"");
            (peer_full->channel.title && strlen(peer_full->channel.title)) ? sprintf(peer_username, "\"%s\"", peer_full->channel.title) : sprintf(peer_username, "null");
            sprintf(peer_firstname, "null");
            sprintf(peer_lastname, "null");
            sprintf(peer_phone, "null");
            get_tgl_file_json(peer_full->channel.photo_big, photo_big_json);
            get_tgl_file_json(peer_full->channel.photo_small, photo_small_json);
            sprintf(peer_photo, "{\"photo_big\":%s, \"photo_small\":%s}", photo_big_json, photo_small_json);
            break;
    }
    sprintf(out,
            "{\"id\":%d, \"type\":%s, \"username\":%s, \"firstname\":%s, \"lastname\":%s, \"phone\":%s, \"photo\":%s}",
            peer.peer_id, peer_type, peer_username, peer_firstname, peer_lastname, peer_phone, peer_photo);
}